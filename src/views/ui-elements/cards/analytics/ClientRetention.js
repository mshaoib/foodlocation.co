import React from "react"
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap"
import Chart from "react-apexcharts"

class ClientRetention extends React.Component {
  state = {
    options: {
      chart: {
        stacked: true,
        toolbar: { show: false }
      },
      plotOptions: {
        bar: {
          columnWidth: "10%"
        }
      },
      colors: [this.props.primary, this.props.danger],
      dataLabels: {
        enabled: false
      },
      grid: {
        borderColor: this.props.labelColor,
        padding: {
          left: 0,
          right: 0
        }
      },
      legend: {
        show: true,
        position: "top",
        horizontalAlign: "left",
        offsetX: 0,
        fontSize: "14px",
        markers: {
          radius: 50,
          width: 10,
          height: 10
        }
      },
      xaxis: {
        labels: {
          style: {
            colors: this.props.strokeColor
          }
        },
        axisTicks: {
          show: false
        },
        categories: [

        ],
        axisBorder: {
          show: false
        }
      },
      yaxis: {
        tickAmount: 5,
        labels: {
          style: {
            color: this.props.strokeColor
          }
        }
      },
      tooltip: {
        x: { show: false }
      }
    },
    series: [
      {
        name: ["Products", "Sale"],
        data: [175, 125,]
      },

    ]
  }
  componentWillReceiveProps(nextProps) {
    let name = nextProps.topSalesProductofMonth &&
      nextProps.topSalesProductofMonth.length &&
      nextProps.topSalesProductofMonth.map((item, i) => item[i] = item.productName)
    let data = nextProps.topSalesProductofMonth &&
      nextProps.topSalesProductofMonth.length &&
      nextProps.topSalesProductofMonth.map((item, i) => item[i] = item.saleQty)

    let series = [{ name, data }]
    this.setState({ series })
  }
  getRandomColor() {
    var lum = -0.25;
    var hex = String('#' + Math.random().toString(16).slice(2, 8).toUpperCase()).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    var rgb = "#",
      c, i;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ("00" + c).substr(c.length);
    }
    return rgb
  }
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Product SAles</CardTitle>
        </CardHeader>
        <CardBody>
          <Chart
            options={this.state.options}
            series={this.state.series}
            type="bar"
            height={290}
            id="client-retention-chart"
          />
        </CardBody>
      </Card>
    )
  }
}
export default ClientRetention
