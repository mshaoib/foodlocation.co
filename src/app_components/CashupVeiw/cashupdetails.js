import React, { useEffect, useState } from 'react'
import { gridDataByClient, deleteapi, getList, report } from '../../API_Helpers/api'
import { Card, CardHeader, CardTitle, CardBody, Button, Row, Col, Label } from "reactstrap"
import { useSelector } from "react-redux"
import message from '../../API_Helpers/toast'
import axios from 'axios'
import moment from 'moment'
import Radio from "../../components/@vuexy/radio/RadioVuexy"
import { history } from '../../history'

import Flatpickr from "react-flatpickr";
import { Download } from 'react-feather'


function Reports(props) {
    const [products, setProducts] = useState([])
    const [product, setProduct] = useState('')
    const [fromDate, setFromDate] = useState('')
    const [toDate, setToDate] = useState('')
    const [reportType, setReportType] = useState('')
    const user = useSelector(state => state.auth.login.user)
    const client = useSelector(state => state.auth.login.client)
    const [loading, setLoading] = React.useState(false)
    const cashupView = useSelector(state => state.updatescreens.cashup)

    const download = () => {
        setLoading(true)
        let data = {
            product,
            fromDate,
            toDate,
            clientId: client.clientId,
            lang: 'EN'
        }
        let authOptions = {
            data: data,
            apiname: reportType,
            tokenType: user.tokenType,
            accessToken: user.accessToken,
        }
        report(authOptions)
            .then(res => {
                setLoading(false)
                const url = window.URL.createObjectURL(new Blob([res.data]));
                const link = document.createElement('a');
                link.href = url;
                let a = new Date()
                let date = a.getDate() + '-' + (a.getMonth() + 1) + '-' + a.getFullYear() + '-' + a.getHours() + ':' + a.getMinutes() + ':' + a.getSeconds()
                link.setAttribute('download', `${reportType}-${date}.pdf`);
                document.body.appendChild(link);
                link.click();
            })
            .catch(err => {

            })
    }
    useEffect(() => {
        getProducts()
    }, [])
    const getProducts = () => {
        let payload = {
            tokenType: user.tokenType,
            accessToken: user.accessToken,
            apiname: "productsList",
            data: {
                clientId: client.clientId,
                lang: "EN",
            },
        }
        getList(payload)
            .then(res => {
                message(res)

                setProducts(res.data.map(i => { return { value: i.id, label: i.name } }))

            })
    }
    const onChange = value => {
        setProduct(value)
    }
    const reports = () => {
        let data = {
            cashupId: cashupView.id,
            clientId: client.clientId,
            lang: 'EN'
        }
        let authOptions = {
            data: data,
            apiname: 'cashupDetailsPdfReport',
            tokenType: user.tokenType,
            accessToken: user.accessToken,
        }
        report(authOptions)
        .then(res => {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            let a = new Date()
            let date = a.getDate() + '-' + (a.getMonth() + 1) + '-' + a.getFullYear()
            link.setAttribute('download', `CashUpReport${date}.pdf`);
            document.body.appendChild(link);
            link.click();
        })
        .catch(err => console.log(err))
    }
    return (
        <Row>
            <Col md="12" sm="12">
                <Card>
                    <CardHeader>
                        <CardTitle style={{ textAlign: "center", width: '100%' }}>Cashup Veiw Details</CardTitle>
                        <Button.Ripple color="primary" onClick={() => reports()}>Download Report</Button.Ripple>
                        <Button.Ripple color="primary" onClick={() => {
                            history.push('/dashboard/cashupview')
                        }}>Back</Button.Ripple>
                    </CardHeader>
                    <CardBody>
                        <>
                            <Row>
                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Cash Up Date</h5>
                                    <p>{cashupView.cashupDate && cashupView.cashupDate.split(" ")[0]}</p>
                                </Col>

                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Cash Up Close Date</h5>
                                    <p>{cashupView.closeDate && cashupView.closeDate.split(" ")[0]}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Cash Amount</h5>
                                    <p>{cashupView.cashMthdAmt && cashupView.cashMthdAmt}</p>
                                </Col>

                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Card Amount</h5>
                                    <p>{cashupView.cardMthdAmt && cashupView.cardMthdAmt}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Total</h5>
                                    <p>{cashupView.total && cashupView.total}</p>
                                </Col>

                                <Col md="6" sm="6">
                                    <h5 className="mt-1">Status</h5>
                                    <p>   {cashupView.status == "O" ? "Open" : "Close"}</p>
                                </Col>
                            </Row>
                        </>
                    </CardBody>
                </Card></Col>
        </Row>
    )
}

export default Reports