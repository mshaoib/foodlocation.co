import React, { useEffect, useState } from 'react'
import { gridDataByClient, deleteapi, getList, report } from '../../API_Helpers/api'
import { Card, CardHeader, CardTitle, CardBody, Button, Row, Col, Label } from "reactstrap"
import { useSelector } from "react-redux"
import message from '../../API_Helpers/toast'
import axios from 'axios'
import moment from 'moment'
import Radio from "../../components/@vuexy/radio/RadioVuexy"

import Flatpickr from "react-flatpickr";
import { Download } from 'react-feather'


function Reports(props) {
    const [products, setProducts] = useState([])
    const [product, setProduct] = useState('')
    const [fromDate, setFromDate] = useState('')
    const [toDate, setToDate] = useState('')
    const [reportType, setReportType] = useState('')
    const user = useSelector(state => state.auth.login.user)
    const client = useSelector(state => state.auth.login.client)
    const [loading, setLoading] = React.useState(false)

    const download = () => {
        setLoading(true)
        let data = {
            product,
            fromDate,
            toDate,
            clientId: client.clientId,
            lang: 'EN'
        }
        let authOptions = {
            data: data,
            apiname: reportType,
            tokenType: user.tokenType,
            accessToken: user.accessToken,
        }
        report(authOptions)
            .then(res => {
                setLoading(false)
                const url = window.URL.createObjectURL(new Blob([res.data]));
                const link = document.createElement('a');
                link.href = url;
                let a = new Date()
                let date = a.getDate() + '-' + (a.getMonth() + 1) + '-' + a.getFullYear() + '-' + a.getHours() + ':' + a.getMinutes() + ':' + a.getSeconds()
                link.setAttribute('download', `${reportType}-${date}.pdf`);
                document.body.appendChild(link);
                link.click();
            })
            .catch(err => {

            })
    }
    useEffect(() => {
        getProducts()
    }, [])
    const getProducts = () => {
        let payload = {
            tokenType: user.tokenType,
            accessToken: user.accessToken,
            apiname: "productsList",
            data: {
                clientId: client.clientId,
                lang: "EN",
            },
        }
        getList(payload)
            .then(res => {
                message(res)

                setProducts(res.data.map(i => { return { value: i.id, label: i.name } }))

            })
    }
    const onChange = value => {
        setProduct(value)
    }
    return (
        <Row>
            <Col md="6" sm="12">
                <Card>
                    <CardHeader>
                        <CardTitle style={{ textAlign: "center", width: '100%' }}>Product Reports</CardTitle>
                    </CardHeader>
                    <CardBody>
                        <>
                            <h5 className="mt-1">Select Product</h5>

                            <Row>
                                <Col md="12" sm="12">
                                    <select
                                        style={{ width: '100%' }}
                                        onChange={event => onChange(event.target.value)}
                                        value={product}>
                                        <option value="">Show All</option>
                                        {
                                            products.length > 0 && products.map(i => (
                                                <option value={i.value}>{i.label}</option>
                                            ))
                                        }
                                    </select>

                                </Col>
                            </Row>
                            <Row>
                                <Col md="12" sm="12">
                                    <h5 className="mt-1">From Date</h5>

                                    <Row>
                                        <Col md="12" sm="12">
                                            <Flatpickr
                                                options={{
                                                    dateFormat: "Y-m-d",
                                                }}
                                                className="form-control"
                                                value={fromDate ? fromDate : null}
                                                onChange={date => setFromDate(moment(date[0]).format("YYYY-MM-DD"))}
                                            />

                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                            <Row>
                                <Col md="12" sm="12">
                                    <h5 className="mt-1">To Date</h5>

                                    <Row>
                                        <Col md="12" sm="12">
                                            <Flatpickr
                                                options={{
                                                    dateFormat: "Y-m-d",
                                                }}
                                                className="form-control"
                                                value={toDate ? toDate : null}
                                                onChange={date => setToDate(moment(date[0]).format("YYYY-MM-DD"))}
                                            />

                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                            <Row>
                                <Col md="12" sm="12">
                                    <Row>
                                        <Col md="12" sm="12">
                                            <h5 className="mt-1">Select Report</h5>
                                            <div className="d-inline-block mr-1">
                                                <Radio
                                                    label="Product Tax Report"
                                                    color="primary"
                                                    checked={reportType == 'productTaxPdfReport' ? true : false}
                                                    value="productTaxPdfReport"

                                                    name="themeMode"
                                                    onClick={(v) => setReportType('productTaxPdfReport')}
                                                    onChange={(v) => setReportType('productTaxPdfReport')}
                                                />
                                                <Radio
                                                    label="Product Tax Details Report"
                                                    color="primary"
                                                    value="productTaxDetailPdfReport"
                                                    checked={reportType == 'productTaxDetailPdfReport' ? true : false}
                                                    name="themeMode"
                                                    onChange={(v) => setReportType('productTaxDetailPdfReport')}
                                                    onClick={(v) => setReportType('productTaxDetailPdfReport')}
                                                />
                                            </div>

                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                            <Button color="primary" onClick={() => download()}>Download</Button>
                        </>
                    </CardBody>
                </Card></Col>
        </Row>
    )
}

export default Reports